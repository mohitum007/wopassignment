package com.mohitum.wopassignment.dataSource

import com.mohitum.wopassignment.dataSource.login.LoginRequest
import com.mohitum.wopassignment.dataSource.login.LoginResponse
import com.mohitum.wopassignment.model.Story
import io.reactivex.rxjava3.core.Observable
import retrofit2.http.*

/**
 * Retrofit API interface. It will have methods for calls to APIs
 */
interface IApiService {

    @Headers(NetworkConstants.HEADER_CONTENT_TYPE_JSON)
    @POST(NetworkConstants.APIEndPoints.LOGIN)
    fun login(@Body loginRequest: LoginRequest): Observable<LoginResponse>

    @Headers(NetworkConstants.HEADER_CONTENT_TYPE_JSON)
    @GET(NetworkConstants.APIEndPoints.GET_TOP_STORIES)
    fun getTopStories(): Observable<List<Long>>

    @Headers(NetworkConstants.HEADER_CONTENT_TYPE_JSON)
    @GET(NetworkConstants.APIEndPoints.GET_STORY_DETAIL)
    fun getStoryDetail(@Path("item_id") itemId: String): Observable<Story>
}