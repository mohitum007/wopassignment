package com.mohitum.wopassignment.dataSource.login

import com.google.gson.annotations.SerializedName

/**
 * Data class for the API error response
 */
data class ErrorResponse(
    // API error code key from response
    @SerializedName("error")
    var error: String? = null,
    // Error description from response
    @SerializedName("description")
    var description: String? = null
)