package com.mohitum.wopassignment.dataSource.topStories

import androidx.lifecycle.MutableLiveData
import com.mohitum.wopassignment.dataSource.IApiService
import com.mohitum.wopassignment.dataSource.NetworkAPIClient.getApiClient
import com.mohitum.wopassignment.model.Story
import com.mohitum.wopassignment.viewModel.MainViewModel
import io.reactivex.rxjava3.core.Observable

/**
 * Network API Call Class for getting stories API
 *
 * @param mainViewModel main view model reference
 */
class StoriesNetworkAPICall(private val mainViewModel: MainViewModel) {

    fun execute(apiCommData: MutableLiveData<ArrayList<Story>>) {
        with(getApiClient().create(IApiService::class.java)) {
            getStoryIds(this)
                .flatMap { itemList ->
                    if (mainViewModel.allStories.isNullOrEmpty()) {
                        mainViewModel.allStories = itemList as ArrayList<Long>
                        val lastIndex = mainViewModel.lastIndex + PAGINATION_ITEM_COUNT - 1
                        val subList = mainViewModel.allStories.subList(mainViewModel.lastIndex, lastIndex)
                        mainViewModel.lastIndex = lastIndex
                        Observable.fromIterable(subList)
                    } else {
                        Observable.fromIterable(itemList)
                    }
                }
                .flatMap { item ->
                    getStoryDetail(item.toString())
                }
                .toList()
                .subscribe({
                        apiCommData.postValue(it as ArrayList<Story>)
                    },
                    {
                        apiCommData.postValue(ArrayList())
                    }
                )
        }
    }

    /**
     * Get the Observable for next page of story ids to get the details of.
     */
    private fun getStoryIds(apiClient: IApiService) =
        if (mainViewModel.allStories.isNullOrEmpty()) {
            // First time loading
            apiClient.getTopStories()
        } else {
            // Get next page data
            val lastIndex = mainViewModel.lastIndex + PAGINATION_ITEM_COUNT - 1
            val subList = mainViewModel.allStories.subList(mainViewModel.lastIndex, lastIndex)
            mainViewModel.lastIndex = lastIndex
            Observable.just(subList)
        }

    companion object {

        /**
         * Constant for pagination items count
         */
        const val PAGINATION_ITEM_COUNT = 20
    }
}