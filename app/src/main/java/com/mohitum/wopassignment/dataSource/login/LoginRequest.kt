package com.mohitum.wopassignment.dataSource.login

import com.google.gson.annotations.SerializedName

/**
 * Data class for Login API Request data
 */
data class LoginRequest(
    // Login username
    @SerializedName("username")
    var username: String?,
    // Login user password
    @SerializedName("password")
    var password: String?
)