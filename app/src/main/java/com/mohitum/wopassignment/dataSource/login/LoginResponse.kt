package com.mohitum.wopassignment.dataSource.login

import com.google.gson.annotations.SerializedName

/**
 * Data class for the Login API response
 */
data class LoginResponse(
    // API Access token from response
    @SerializedName("token")
    var token: String? = null,
    // API error response
    @SerializedName("errorResponse")
    var errorResponse: ErrorResponse? = null
)