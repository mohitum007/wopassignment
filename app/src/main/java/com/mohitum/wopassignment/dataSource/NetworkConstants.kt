package com.mohitum.wopassignment.dataSource

/**
 * Network Layer constants.
 * All the API specific constants will be defined here.
 */
object NetworkConstants {
    /**
     * Constants for the OkHttpClient
     * used to set the request read and connection timeout in minutes
     * for each request
     */
    const val READ_TIMEOUT: Long = 3
    const val CONNECTION_TIMEOUT: Long = 3

    /**
     * API Base URL
     */
    const val BASE_URL: String = "https://hacker-news.firebaseio.com/v0/"

    /**
     * API headers
     */
    const val HEADER_CONTENT_TYPE_TITLE = "Content-Type"
    const val HEADER_JSON = "application/json"
    const val HEADER_CONTENT_TYPE_JSON = "$HEADER_CONTENT_TYPE_TITLE: $HEADER_JSON"

    /**
     * API end points
     */
    object APIEndPoints {
        // Login API
        const val LOGIN = "login.json"

        // Get top stories API
        const val GET_TOP_STORIES = "topstories.json"

        // Get story details API
        const val GET_STORY_DETAIL = "item/{item_id}.json"
    }
}