package com.mohitum.wopassignment.dataSource.login

import com.google.gson.Gson
import com.mohitum.wopassignment.BuildConfig
import com.mohitum.wopassignment.dataSource.NetworkConstants
import com.mohitum.wopassignment.dataSource.NetworkConstants.HEADER_CONTENT_TYPE_TITLE
import com.mohitum.wopassignment.dataSource.NetworkConstants.HEADER_JSON
import com.mohitum.wopassignment.utils.AppConstants.DEF_USERNAME
import com.mohitum.wopassignment.utils.AppConstants.DEF_USER_PASSWORD
import okhttp3.Interceptor
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.Protocol
import okhttp3.Request
import okhttp3.Response
import okhttp3.ResponseBody.Companion.toResponseBody
import okio.Buffer
import java.net.URI

/**
 * Interceptor class for mocking the login API response
 */
class MockLoginInterceptor : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        val response: Response
        if (BuildConfig.DEBUG) {
            // Get Request URI.
            val uri: URI = chain.request().url.toUri()
            // Check for login API type
            if (uri.toString() == NetworkConstants.BASE_URL + NetworkConstants.APIEndPoints.LOGIN) {
                var responseString: String
                var responseCode: Int
                try {
                    val requestBody =
                        Gson().fromJson(bodyToString(chain.request()), LoginRequest::class.java)
                    // Check for valid credentials - test@worldofplay.in / Worldofplay@2020
                    if (requestBody.username == DEF_USERNAME &&
                        requestBody.password == DEF_USER_PASSWORD
                    ) {
                        // Success
                        responseString = LOGIN_RESPONSE_200
                        responseCode = 200
                    } else {
                        // Invalid credentials
                        responseString = LOGIN_RESPONSE_401
                        responseCode = 401
                    }
                } catch (e: Exception) {
                    // Bad request data
                    responseString = LOGIN_RESPONSE_400
                    responseCode = 400
                }
                response = Response.Builder()
                    .code(responseCode)
                    .message(responseString)
                    .request(chain.request())
                    .protocol(Protocol.HTTP_1_0)
                    .body(
                        responseString.toByteArray()
                            .toResponseBody(HEADER_JSON.toMediaType())
                    )
                    .addHeader(HEADER_CONTENT_TYPE_TITLE, HEADER_JSON)
                    .build()
            } else {
                response = chain.proceed(chain.request())
            }
        } else {
            response = chain.proceed(chain.request())
        }
        return response
    }

    private fun bodyToString(request: Request): String? {
        val copy = request.newBuilder().build()
        val buffer = Buffer()
        copy.body?.writeTo(buffer)
        return buffer.readUtf8()
    }

    companion object {
        /**
         * Mock data for successful API response. Response 200
         */
        private const val LOGIN_RESPONSE_200 = "{\"token\":\"VwvYXBpXC9\"}"

        /**
         * Mock data for invalid credentials API response. Response 401
         */
        private const val LOGIN_RESPONSE_401 =
            "{\"error\":\"invalid_credentials\", \"description\": \"Email address and password is not a valid combination.\"}"

        /**
         * Mock data for Bad API request data. Response 400
         */
        private const val LOGIN_RESPONSE_400 =
            "{\"error\":\"bad_request\", \"description\": \"Network communication error.\"}"
    }

}