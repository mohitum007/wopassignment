package com.mohitum.wopassignment.dataSource.login

import androidx.lifecycle.MutableLiveData
import com.google.gson.Gson
import com.mohitum.wopassignment.dataSource.IApiService
import com.mohitum.wopassignment.dataSource.NetworkAPIClient
import retrofit2.HttpException

/**
 * Network API Call for user login API using email and password
 *
 * @property loginRequest login request data
 */
class LoginNetworkAPICall(private val loginRequest: LoginRequest) {

    /**
     * Method to execute the login API with live data object to listen callback data updates
     *
     * @param apiCommData live data object to listen callback data updates
     */
    fun execute(apiCommData: MutableLiveData<LoginResponse>) {
        with(NetworkAPIClient.getApiClient().create(IApiService::class.java)) {
            login(loginRequest)
                .subscribe(
                    {
                        apiCommData.postValue(it)
                    },
                    { error ->
                        // Create error response and post to live data
                        apiCommData.postValue(LoginResponse().apply {
                            errorResponse = Gson().fromJson(
                                (error as HttpException).response()?.message(),
                                ErrorResponse::class.java
                            )
                        })
                    }
                )
        }
    }
}