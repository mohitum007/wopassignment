package com.mohitum.wopassignment.dataSource

import com.mohitum.wopassignment.dataSource.NetworkConstants.BASE_URL
import com.mohitum.wopassignment.BuildConfig
import com.mohitum.wopassignment.dataSource.login.MockLoginInterceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava3.RxJava3CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

/**
 * Used for creating a Network client instance used for API communications
 * This class will also used to fetching the error messages for API error codes using a config file.
 */
object NetworkAPIClient {

    /**
     * Variable to hold the retrofit instance
     */
    private lateinit var retrofit: Retrofit

    /**
     * Method used to get or create the retrofit instance
     *
     * @return retrofit singleton instance
     */
    fun getApiClient() = if (isRetrofitInitialized()) {
        retrofit
    } else {
        // Not initialized, create new instance
        retrofit = Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava3CallAdapterFactory.create())
            .client(createOkHttpClient())
            .build()
        retrofit
    }

    /**
     * Method to check if [retrofit] instance available and initialized or
     * if the base url is different from the previous url (in case of environment change)
     *
     * @return true if retrofit is available and initialized with current regions base url
     */
    private fun isRetrofitInitialized(): Boolean {
        if (NetworkAPIClient::retrofit.isInitialized) {
            // Check for base url in current API environment
            return BASE_URL == retrofit.baseUrl().toUri().toString()
        }
        return false
    }

    /**
     * Method to create an [OkHttpClient] for intercepting the API client request.
     * This client will take care of API timeouts and the debug logging
     *
     * @return [OkHttpClient] instance with interceptor
     */
    private fun createOkHttpClient(): OkHttpClient = OkHttpClient.Builder()
        .readTimeout(NetworkConstants.READ_TIMEOUT, TimeUnit.MINUTES)
        .connectTimeout(
            NetworkConstants.CONNECTION_TIMEOUT,
            TimeUnit.MINUTES
        )
        .addInterceptor(HttpLoggingInterceptor().apply {
            // set your desired log level
            level = if (BuildConfig.DEBUG) {
                HttpLoggingInterceptor.Level.BODY
            } else {
                HttpLoggingInterceptor.Level.NONE
            }
        })
        .addInterceptor(MockLoginInterceptor())
        .build()
}