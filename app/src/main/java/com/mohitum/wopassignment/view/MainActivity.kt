package com.mohitum.wopassignment.view

import android.content.Context
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.mohitum.wopassignment.R
import com.mohitum.wopassignment.utils.AppConstants.THEME_PREF_NAME
import com.mohitum.wopassignment.view.base.AppProgressView
import com.mohitum.wopassignment.viewModel.MainViewModel

/**
 * Activity class to main single screen component which will contain all the fragments
 */
class MainActivity : AppCompatActivity() {

    /**
     * View Model instance for the activity
     */
    lateinit var mainViewModel: MainViewModel

    /**
     * View instance for loading view in the activity
     */
    private val appProgressView = AppProgressView()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val sharedPreferences = getSharedPreferences(
            THEME_PREF_NAME,
            Context.MODE_PRIVATE
        )
        if (sharedPreferences.getBoolean("isTheme1Selected", true)) {
            setTheme(R.style.AppTheme1)
        } else {
            setTheme(R.style.AppTheme2)
        }
        setContentView(R.layout.activity_main)
        mainViewModel = ViewModelProvider(this)[MainViewModel::class.java]
        addFragment(LoginFragment(), R.id.fl_container, false)
    }

    /**
     * Method to show progress view loader for timed tasks
     *
     * @param message message to be displayed with loader
     */
    fun showLoader(message: String? = null) {
        appProgressView.show(this, message)
    }

    /**
     * Method to dismiss progress view loader if showing
     */
    fun dismissLoader() {
        appProgressView.dismiss()
    }

    /**
     * Method to perform a replace transaction on [container] with provided [fragment]
     *
     * @param fragment the fragment to replace in [container]
     * @param container the container frame layout resource id
     * @param addToBackStack true if we need to add [fragment] in back stack
     */
    fun replaceFragment(fragment: Fragment, container: Int, addToBackStack: Boolean) {
        inflateFragment(
            fragment, container, addToBackStack, TransactionType.REPLACE
        )
    }

    /**
     * Method to perform a add transaction on [container] with provided [fragment]
     *
     * @param fragment the fragment to add in [container]
     * @param container the container frame layout resource id
     * @param addToBackStack true if we need to add [fragment] in back stack
     */
    fun addFragment(fragment: Fragment, container: Int, addToBackStack: Boolean) {
        inflateFragment(
            fragment, container, addToBackStack, TransactionType.ADD
        )
    }

    /**
     * Method to inflate the [fragment] in the [container] with the [transactionType]
     *
     * @param fragment the fragment to inflate in [container]
     * @param container the container frame layout resource id
     * @param addToBackStack true if we need to add [fragment] in back stack
     * @param transactionType fragment transaction, can be [TransactionType.ADD] or [TransactionType.REPLACE]
     */
    private fun inflateFragment(
        fragment: Fragment,
        container: Int,
        addToBackStack: Boolean,
        transactionType: TransactionType
    ) {
        val transaction = supportFragmentManager.beginTransaction()
        val tag = fragment.javaClass.simpleName
        if (addToBackStack)
            transaction.addToBackStack(tag)
        when (transactionType) {
            TransactionType.REPLACE -> transaction.replace(container, fragment, tag)
            TransactionType.ADD -> transaction.add(container, fragment, tag)
        }
        transaction.commitAllowingStateLoss()
    }

    /**
     * Enum for Fragment transaction type to inflate one.
     *
     * @property ADD will be used to add a fragment in given container
     * @property REPLACE will be used to replace current fragment from given container
     */
    enum class TransactionType {
        REPLACE, ADD
    }
}