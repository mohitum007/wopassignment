package com.mohitum.wopassignment.view.base

import android.graphics.Canvas
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import androidx.recyclerview.widget.RecyclerView

/**
 * Custom item decoration class for adding colored dividers to the recycler view items
 *
 * @param color divider color
 * @property padding horizontal padding to be added to divider
 */
class DividerItemDecoration(color: Int, private val padding: Int) :
    RecyclerView.ItemDecoration() {

    /**
     * Drawable for the divider view
     */
    private var divider: Drawable = ColorDrawable(color)

    override fun onDraw(canvas: Canvas, parent: RecyclerView, state: RecyclerView.State) {
        for (index in 0 until parent.childCount) {
            // Get child view at index
            with(parent.getChildAt(index)) {
                val top = this.bottom + (this.layoutParams as RecyclerView.LayoutParams).bottomMargin
                // Set divider layout bounds
                divider.setBounds(
                    padding,
                    top,
                    parent.width - padding,
                    top + divider.intrinsicHeight
                )
                divider.draw(canvas)
            }
        }
    }
}