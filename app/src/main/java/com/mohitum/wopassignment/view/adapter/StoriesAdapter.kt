package com.mohitum.wopassignment.view.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.mohitum.wopassignment.R
import com.mohitum.wopassignment.model.Story
import kotlinx.android.synthetic.main.item_story.view.*

/**
 * RecyclerView Adapter class for loading stories
 *
 * @property activityContext associated and calling component
 * @property stories list of story items
 * @property onOptionClick item click callback
 */
class StoriesAdapter(
    private val activityContext: Context,
    private val stories: ArrayList<Story>,
    private val onOptionClick: ((Story) -> Unit)
) : RecyclerView.Adapter<StoriesAdapter.StoryViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        StoryViewHolder(
            LayoutInflater.from(activityContext)
                .inflate(R.layout.item_story, parent, false)
        )

    override fun getItemCount() = stories.size

    override fun onBindViewHolder(holder: StoryViewHolder, position: Int) {
        with(stories[position]) {
            holder.itemView.apply {
                tv_title.text = this@with.title
                tv_subtitle.text = this@with.type
                setOnClickListener {
                    onOptionClick.invoke(this@with)
                }
            }
        }
    }

    /**
     * View holder class to store and recycle views as they are scrolled off screen
     *
     * @param view item view for the view holder
     */
    inner class StoryViewHolder(view: View) : RecyclerView.ViewHolder(view)
}
