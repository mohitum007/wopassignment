package com.mohitum.wopassignment.view.base

import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.view.LayoutInflater
import android.view.View
import android.widget.TextView
import com.mohitum.wopassignment.R

/**
 * Custom class for application progress view loader
 */
class AppProgressView {

    /**
     * Progress dialog instance
     */
    private var dialog: Dialog? = null

    /**
     * Method to show the loader view with custom message and other parameters
     *
     * @param context calling component context
     * @param title progress dialog text
     * @param cancelable true if cancelable dialog text
     * @param cancelListener callbeck when dialog is cancelled
     *
     * @return loader [dialog]
     */
    fun show(
        context: Context, title: CharSequence? = null, cancelable: Boolean = false,
        cancelListener: DialogInterface.OnCancelListener? = null
    ): Dialog? {
        val view: View = (context
            .getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater).inflate(
            R.layout.view_app_progress,
            null
        )
        if (title != null) {
            val tvLoaderMessage = view.findViewById(R.id.tv_loader_message) as TextView?
            tvLoaderMessage?.text = title
        }
        dialog = Dialog(context, R.style.AppProgressView)
        dialog?.setContentView(view)
        dialog?.setCancelable(cancelable)
        dialog?.setOnCancelListener(cancelListener)
        dialog?.show()
        return dialog
    }

    /**
     * Method to check if the dialog is showing or not
     *
     * @return true if showing else false
     */
    fun isShowing() = dialog?.isShowing ?: false

    /**
     * Method to dismiss the dialog if showing
     */
    fun dismiss() {
        if (isShowing()) {
            dialog?.dismiss()
        }
    }
}