package com.mohitum.wopassignment.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.mohitum.wopassignment.R
import com.mohitum.wopassignment.databinding.FragmentDashboardBinding
import com.mohitum.wopassignment.utils.Utility
import com.mohitum.wopassignment.view.DetailPageFragment.Companion.STORY_DATA
import com.mohitum.wopassignment.view.adapter.StoriesAdapter
import com.mohitum.wopassignment.view.base.BaseFragment
import com.mohitum.wopassignment.view.base.DividerItemDecoration
import kotlinx.android.synthetic.main.fragment_dashboard.*

/**
 * Fragment class for the Dashboard screen
 */
class DashboardFragment : BaseFragment<FragmentDashboardBinding>() {

    /**
     * Lazy instance of the [StoriesAdapter]
     */
    private val storiesAdapter: StoriesAdapter by lazy {
        StoriesAdapter(
            getBaseActivity(),
            mainViewModel.topStories
        ) {
            val fragment =  DetailPageFragment()
            fragment.arguments = Bundle().apply {
                putParcelable(STORY_DATA, it)
            }
            getBaseActivity().replaceFragment(fragment, R.id.fl_container, true)
        }
    }

    /**
     * Boolean to check if load more data is required for pagination or not
     */
    private var isLoadMoreRequired = true

    /**
     * Previous visible items count in recycler view
     */
    private var pastVisibleItems = 0

    /**
     * Current visible items count in recycler view
     */
    private var visibleItemCount:Int = 0

    /**
     * Total items in recycler view
     */
    private var totalItemCount:Int = 0

    override fun getViewLayoutResId(
        inflater: LayoutInflater,
        container: ViewGroup?
    ): FragmentDashboardBinding = DataBindingUtil.inflate(
        inflater, R.layout.fragment_dashboard, container, false
    )

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        // Set initial view properties
        setInitialView()
        // Set up view data observers
        setDataObservers()
        if (Utility.isNetworkAvailable(context)) {
            getBaseActivity().showLoader()
            mainViewModel.getTopStoriesFromServer()
        } else {
            // Internet error
            Utility.showAlert(requireContext(), getString(R.string.error_no_internet)) {
                getBaseActivity().onBackPressed()
            }
        }
    }

    /**
     * Method to set initial view properties
     */
    private fun setInitialView() {
        val mLayoutManager = LinearLayoutManager(getBaseActivity())
        rv_stories.layoutManager = mLayoutManager
        rv_stories.addItemDecoration(
            DividerItemDecoration(
                ContextCompat.getColor(
                    getBaseActivity(),
                    R.color.colorDivider
                ), 0
            )
        )
        rv_stories.adapter = storiesAdapter
        rv_stories.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                if (dy > 0) { //check for scroll down
                    visibleItemCount = mLayoutManager.childCount
                    totalItemCount = mLayoutManager.itemCount
                    pastVisibleItems = mLayoutManager.findFirstVisibleItemPosition()
                    if (isLoadMoreRequired && visibleItemCount + pastVisibleItems >= totalItemCount) {
                        if (Utility.isNetworkAvailable(context)) {
                            isLoadMoreRequired = false
                            pb_load_more.visibility = VISIBLE
                            // Fetch next page data
                            mainViewModel.getTopStoriesFromServer()
                        } else {
                            // Internet error
                            Utility.showAlert(requireContext(), getString(R.string.error_no_internet))
                        }
                    }
                }
            }
        })
    }

    /**
     * Set up view data observers
     */
    private fun setDataObservers() {
        mainViewModel.topStoriesAPILiveData.observe(viewLifecycleOwner, Observer { apiCommData ->
            getBaseActivity().dismissLoader()
            if (!apiCommData.isNullOrEmpty()) {
                mainViewModel.topStories.addAll(apiCommData)
                storiesAdapter.notifyDataSetChanged()
                pb_load_more.visibility = GONE
                // Check for load more flag
                isLoadMoreRequired = mainViewModel.topStories.size < mainViewModel.allStories.size
            } else {
                // Response failed. Show error
                Utility.showAlert(requireContext(), getString(R.string.error_default))
            }
        })
    }
}