package com.mohitum.wopassignment.view

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.webkit.WebChromeClient
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import com.mohitum.wopassignment.R
import com.mohitum.wopassignment.model.Story
import com.mohitum.wopassignment.databinding.FragmentDetailPageBinding
import com.mohitum.wopassignment.utils.Utility
import com.mohitum.wopassignment.view.base.BaseFragment
import kotlinx.android.synthetic.main.fragment_detail_page.*

/**
 * Fragment class for the Single story detail screen
 */
class DetailPageFragment : BaseFragment<FragmentDetailPageBinding>() {
    private var story: Story? = null

    override fun getViewLayoutResId(
        inflater: LayoutInflater,
        container: ViewGroup?
    ): FragmentDetailPageBinding = DataBindingUtil.inflate(
        inflater, R.layout.fragment_detail_page, container, false
    )

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        story = arguments?.get(STORY_DATA) as Story?
        // Set initial view properties
        setInitialView()
        if (Utility.isNetworkAvailable(context)) {
            wv_detail.loadUrl(story?.url)
        } else {
            // Internet error
            Utility.showAlert(requireContext(), getString(R.string.error_no_internet)) {
                getBaseActivity().onBackPressed()
            }
        }
    }

    /**
     * Method to set initial view properties
     */
    @SuppressLint("SetJavaScriptEnabled")
    private fun setInitialView() {
        tv_details_title.text = story?.title
        wv_detail.webChromeClient = object : WebChromeClient() {
            override fun onProgressChanged(view: WebView, progress: Int) {
                if (progress == 100) {
                    pb_loader?.visibility = GONE
                } else {
                    pb_loader?.visibility = VISIBLE
                }
            }
        }
        wv_detail.webViewClient = AppWebViewClient()
        wv_detail.settings.javaScriptEnabled = true
    }

    /**
     * Web view client custom inner class
     */
    inner class AppWebViewClient : WebViewClient() {
        override fun shouldOverrideUrlLoading(view: WebView, url: String?): Boolean {
            view.loadUrl(url)
            return true
        }

        override fun onPageFinished(view: WebView?, url: String?) {
            pb_loader?.visibility = GONE
        }

        override fun onReceivedError(
            view: WebView?,
            errorCode: Int,
            description: String,
            failingUrl: String?
        ) {
            pb_loader?.visibility = GONE
            Toast.makeText(requireContext(), "Error:$description", Toast.LENGTH_SHORT).show()
        }
    }

    companion object {
        /**
         * Argument data key constant
         */
        const val STORY_DATA = "STORY_DATA"
    }
}