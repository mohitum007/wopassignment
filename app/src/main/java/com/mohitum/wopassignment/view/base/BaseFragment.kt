package com.mohitum.wopassignment.view.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import com.mohitum.wopassignment.view.MainActivity
import com.mohitum.wopassignment.viewModel.MainViewModel

/**
 * Abstract base class for all fragment screens.
 * This class contain the common components and methods for fragments
 * All fragment classes should extend this base class for common functionality.
 */
abstract class BaseFragment<T: ViewDataBinding> : Fragment() {

    /**
     * Main Activity view model reference
     */
    lateinit var mainViewModel: MainViewModel

    /**
     * Reference to view data Binding of the fragment layout
     */
    lateinit var viewDataBinding: T

    /**
     * Abstract method to provide the layout resource id for current fragment
     *
     * @return layout resource id of the layout
     */
    abstract fun getViewLayoutResId(inflater: LayoutInflater, container: ViewGroup?): T

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        retainInstance = true
        viewDataBinding = getViewLayoutResId(inflater, container)
        return viewDataBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mainViewModel = getBaseActivity().mainViewModel
    }

    /**
     * Method to return the associated activity as [MainActivity].
     *
     * @return the [MainActivity] associated with the fragment
     */
    fun getBaseActivity() = activity as MainActivity

    /**
     * Method to return the associated activity's view model
     *
     * @return the viewModel associated
     */
    fun getViewModel() = getBaseActivity().mainViewModel
}