package com.mohitum.wopassignment.view

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import com.mohitum.wopassignment.R
import com.mohitum.wopassignment.databinding.FragmentLoginBinding
import com.mohitum.wopassignment.utils.AppConstants.IS_THEME_1_SELECTED
import com.mohitum.wopassignment.utils.AppConstants.THEME_PREF_NAME
import com.mohitum.wopassignment.utils.Utility
import com.mohitum.wopassignment.view.base.BaseFragment
import kotlinx.android.synthetic.main.fragment_login.*

/**
 * Fragment class for the Login screen
 */
class LoginFragment : BaseFragment<FragmentLoginBinding>() {

    /**
     * Reference to the application shared preferences
     */
    private lateinit var sharedPreferences: SharedPreferences

    override fun getViewLayoutResId(
        inflater: LayoutInflater,
        container: ViewGroup?
    ): FragmentLoginBinding = DataBindingUtil.inflate(
        inflater, R.layout.fragment_login, container, false
    )

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewDataBinding.mainViewModel = mainViewModel
        sharedPreferences = getBaseActivity().getSharedPreferences(
            THEME_PREF_NAME,
            Context.MODE_PRIVATE
        )
        switch_theme.isChecked = sharedPreferences.getBoolean(IS_THEME_1_SELECTED, true)
        // Set view listeners
        setListeners()
        // Set up view data observers
        setDataObservers()
    }

    /**
     * Set view listeners events
     */
    private fun setListeners() {
        switch_theme.setOnCheckedChangeListener { buttonView, isChecked ->
            if (buttonView.isPressed) {
                if (isChecked) {
                    sharedPreferences.edit().putBoolean(IS_THEME_1_SELECTED, true).apply()
                } else {
                    sharedPreferences.edit().putBoolean(IS_THEME_1_SELECTED, false).apply()
                }
                getBaseActivity().startActivity(Intent(getBaseActivity(), MainActivity::class.java))
                getBaseActivity().finish()
            }
        }
    }

    /**
     * Set up view data observers
     */
    private fun setDataObservers() {
        getViewModel().userMutableLiveData.observe(viewLifecycleOwner, Observer { loginUser ->
            when {
                loginUser.username.isNullOrBlank() -> {
                    viewDataBinding.edtUsername.error = getString(R.string.error_email_empty)
                    viewDataBinding.edtUsername.requestFocus()
                }
                !loginUser.isValidEmail -> {
                    viewDataBinding.edtUsername.error = getString(R.string.error_email_invalid)
                    viewDataBinding.edtUsername.requestFocus()
                }
                loginUser.password.isNullOrBlank() -> {
                    viewDataBinding.edtPassword.error = getString(R.string.error_password_empty)
                    viewDataBinding.edtPassword.requestFocus()
                }
                !loginUser.isValidPassword -> {
                    viewDataBinding.edtPassword.error = getString(R.string.error_password_invalid)
                    viewDataBinding.edtPassword.requestFocus()
                }
                else -> {
                    getBaseActivity().showLoader()
                    mainViewModel.getLogin()
                }
            }
        })
        getViewModel().loginAPILiveData.observe(viewLifecycleOwner, Observer { loginResponse ->
            getBaseActivity().dismissLoader()
            loginResponse.token?.let {
                getBaseActivity().replaceFragment(DashboardFragment(), R.id.fl_container, true)
            } ?: kotlin.run {
                // Response failed. Show error
                Utility.showAlert(requireContext(), loginResponse.errorResponse?.description ?: getString(R.string.error_default))
            }
        })
    }
}