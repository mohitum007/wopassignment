package com.mohitum.wopassignment.utils

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import androidx.appcompat.app.AlertDialog

/**
 * Object for the utility methods
 */
object Utility {

    /**
     * Method to show an alert message to user in dialog
     *
     * @param context calling activity or fragment context
     * @param message message to be displayed in the alert message
     * @param onPositiveButtonClick optional click callback for positive button click
     */
    fun showAlert(context: Context, message: String, onPositiveButtonClick: (() -> Unit)? = null) {
        val dialogBuilder = AlertDialog.Builder(context)
        dialogBuilder.setMessage(message)
            .setCancelable(false)
            .setPositiveButton("OK") { _, _ ->
                onPositiveButtonClick?.invoke()
            }
        dialogBuilder.create().show()
    }

    /**
     * Method to check Internet availability
     *
     * @return true if Internet is available else false
     */
    fun isNetworkAvailable(context: Context?): Boolean {
        @Suppress("DEPRECATION")
        with(context?.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                //Device is running on Marshmallow or later Android OS.
                getNetworkCapabilities(activeNetwork)?.let {
                    return it.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) || it.hasTransport(
                        NetworkCapabilities.TRANSPORT_CELLULAR
                    )
                }
            } else {
                activeNetworkInfo?.let {
                    // connected to the internet
                    return listOf(
                        ConnectivityManager.TYPE_WIFI,
                        ConnectivityManager.TYPE_MOBILE
                    ).contains(it.type)
                }
            }
        }
        return false
    }
}