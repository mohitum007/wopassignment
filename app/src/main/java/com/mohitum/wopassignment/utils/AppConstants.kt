package com.mohitum.wopassignment.utils

/**
 * Application level constants
 */
object AppConstants {

    /**
     * Preference file name for the theme selection
     */
    const val THEME_PREF_NAME = "ThemePref"

    /**
     * Preference key name for the theme selection boolean
     */
    const val IS_THEME_1_SELECTED = "isTheme1Selected"

    /**
     * Default success username
     */
    const val DEF_USERNAME = "test@worldofplay.in"

    /**
     * Default success user password
     */
    const val DEF_USER_PASSWORD = "Worldofplay@2020"
}