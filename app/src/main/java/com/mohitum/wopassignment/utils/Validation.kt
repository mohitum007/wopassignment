package com.mohitum.wopassignment.utils

import android.util.Patterns

/**
 * Utility class for field or object validation checks
 */
object Validation {

    /**
     * Regular expression for password, 1 uppercase, 1 lower case, 1 special, length 8 to 16
     */
    private const val REGEX_PASSWORD =
        "((?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%]).{8,16})"

    /**
     * Check if email string is valid format.
     *
     * @param email input email string
     *
     * @return boolean email format validation
     */
    fun isValidEmail(email: String?) =
        email != null && !isEmpty(email, true) && Patterns.EMAIL_ADDRESS.matcher(email).matches()

    /**
     * Check if password string is valid format.
     *
     * @param password input password string
     *
     * @return boolean password format validation
     */
    fun isValidPassword(password: String?) =
        password != null && !isEmpty(password, true) && isValid(password, REGEX_PASSWORD)

    /**
     * Custom rule by passing regular expression.
     *
     * @param value input string
     * @param regex rule
     *
     * @return boolean true if valid
     */
    private fun isValid(value: String, regex: String) = value.matches(Regex(regex))

    /**
     * Check if object is empty or not, null, blank, 0, false is empty
     * depends on data type or how the object can be casted.
     *
     * @param value needs to be checked
     * @param isIgnoreSpace is include space or not
     *
     * @return boolean true if valid
     */
    private fun isEmpty(value: Any?, isIgnoreSpace: Boolean = false) = if (value == null) {
        true
    } else if (value is Boolean) {
        !(value as Boolean?)!!
    } else if (value is String) {
        if (isIgnoreSpace) {
            value.toString().trim { it <= ' ' }.isEmpty()
        } else value.toString().isEmpty()
    } else {
        try {
            val result = value.toString().toInt()
            result == 0
        } catch (e: NumberFormatException) {
            false
        }
    }
}