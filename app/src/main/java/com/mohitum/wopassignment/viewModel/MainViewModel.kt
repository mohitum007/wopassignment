package com.mohitum.wopassignment.viewModel

import android.app.Application
import android.view.View
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.mohitum.wopassignment.dataSource.login.LoginNetworkAPICall
import com.mohitum.wopassignment.dataSource.login.LoginRequest
import com.mohitum.wopassignment.dataSource.login.LoginResponse
import com.mohitum.wopassignment.model.Story
import com.mohitum.wopassignment.dataSource.topStories.StoriesNetworkAPICall
import com.mohitum.wopassignment.model.LoginUser
import java.util.*

/**
 * View model class for Main Activity screen
 *
 * @param application application context attached
 */
class MainViewModel(application: Application) : AndroidViewModel(application) {

    /**
     * Username live data instance for data binding
     */
    var username = MutableLiveData<String>()

    /**
     * Password live data instance for data binding
     */
    var password = MutableLiveData<String>()

    /**
     * Login user live data instance for data binding
     */
    var userMutableLiveData = MutableLiveData<LoginUser>()

    /**
     * Array list of all the fetched story ids
     */
    var allStories = ArrayList<Long>()

    /**
     * Current available story items
     */
    var topStories = ArrayList<Story>()

    /**
     * Last available story item index for pagination as per [allStories]
     */
    var lastIndex = 0

    /**
     * Live data instance for story list
     */
    var topStoriesAPILiveData = MutableLiveData<ArrayList<Story>>()

    /**
     * Live data instance for [LoginResponse]
     */
    var loginAPILiveData = MutableLiveData<LoginResponse>()

    /**
     * Method invocation for login click from data binding
     */
    fun onLoginClick(view: View?) {
        val loginUser = LoginUser(username.value, password.value)
        userMutableLiveData.value = loginUser
    }

    /**
     * Method to perform login to the API with valid data
     */
    fun getLogin() {
        val loginNetworkAPICall = LoginNetworkAPICall(LoginRequest(username.value, password.value))
        loginNetworkAPICall.execute(loginAPILiveData)
    }

    /**
     * Method to get the top stories with data from API based on pagination index
     */
    fun getTopStoriesFromServer() {
        val topStoriesNetworkAPICall = StoriesNetworkAPICall(this)
        topStoriesNetworkAPICall.execute(topStoriesAPILiveData)
    }
}