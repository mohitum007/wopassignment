package com.mohitum.wopassignment.model

import com.mohitum.wopassignment.utils.Validation

/**
 * Data class for login user data object. This will also be used in validations
 *
 * @param username login user name or email id
 * @param password login user password
 */
data class LoginUser(val username: String?, val password: String?) {

    /**
     * Check for email validations
     */
    val isValidEmail: Boolean
        get() = Validation.isValidEmail(username)

    /**
     * Check for password validations
     */
    val isValidPassword: Boolean
        get() = Validation.isValidPassword(password)

}