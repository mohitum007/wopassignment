package com.mohitum.wopassignment.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

/**
 * Data class to define a single story item
 */
@Parcelize
data class Story(
    @SerializedName("by")
    val by: String? = null,
    @SerializedName("descendants")
    val descendants: Int? = null,
    @SerializedName("id")
    val id: Int? = null,
    @SerializedName("kids")
    val kids: List<Int?>? = null,
    @SerializedName("score")
    val score: Int? = null,
    @SerializedName("time")
    val time: Int? = null,
    @SerializedName("title")
    val title: String? = null,
    @SerializedName("type")
    val type: String? = null,
    @SerializedName("url")
    val url: String? = null
) : Parcelable